#ifndef FT_NM_H
#define FT_NM_H

#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stdlib.h>

#include <stdio.h>

#include "elf.h"
#include "elf_x64.h"
#include "elf_x86_32.h"

#define SUCCESS     0
#define FAILURE     1
#define NB_FLAGS    5

#define FLAG_a      0
#define FLAG_g      1
#define FLAG_u      2
#define FLAG_r      3
#define FLAG_p      4

// nm output : (symbol value) (symbol type) (symbol name)

// A
// B
// C
// D
// G
// I
// N 
// R 
// S 
// T 
// U
// V
// W 
// - 
// ?

typedef struct          s_display {
    struct s_display*   next;
    uint64_t            addr;
    char                letter;
    const char*         name;
}                       t_display;

typedef struct          s_file {
    struct s_file*      next;
    char*               name;
    int                 fd;         /* file descriptor du fichier ouvert */
    void*               addr;       /* adresse memoire du fichier mape */
    long int            size;       /* taille du binaire */
    int                 endian;     /* type d'encodage du binaire */
    t_display*          display;    /* liste contenant les elements affiches par nm */
}                       t_file;

typedef struct          s_nm {
    t_file*             files;
    size_t              nb_files;
}                       t_nm;


/* main.c */
void    check_arguments(char* av, t_nm* nm);

/* utils.c */
void    leave_properply(t_file* file);
void    error_message(char* filename, char* msg, t_file* file);
int		ft_memcmp(const void *s1, const void *s2, size_t n);
void*   ft_memset(void *b, int c, size_t len);
int		ft_strcmp(const char *s1, const char *s2);
int 	ft_charcmp(char a, char b);
int     ft_strlen(char* s);

/* parsing_flags.c */
void 	unrecognized_option(char* flag);
void 	invalid_option(char flag);
int 	quick_check_double_dash_help_flag(int ac, char** av);
int 	check_options_flags(char flag);
int		quick_check_simple_dash_help_flag(int ac, char** av);

/* parsing_elf_header.c */
int     parse_elf_class(const unsigned char* ei_class);
int     parse_elf_endian(const unsigned char* ei_data);
int     parse_elf_version(const unsigned char* version);
int     parse_elf_type(int e_type);
char*   parse_elf_header(t_file* file);

/* elf_x64_handler.c */
int     elf_x64_symbols_handler(t_elf_64* elf, t_file* file, int symtab_id);
int     elf_x64_sections_handler(t_file* file, t_elf_64* elf);
char*   elf_x64_handler(t_file* file);

/* elf_x86_32_handler.c */
char*   elf_x86_32_handler(t_file* file);

#endif
