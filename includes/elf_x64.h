#ifndef ELF_X64_H
#define ELF_X64_H

#include "elf.h"

typedef struct      s_elf_64 {
    Elf64_Ehdr*     ehdr;
    Elf64_Shdr*     shdr;
    Elf64_Phdr*     phdr;
}                   t_elf_64;

#endif
