#include "../includes/ft_nm.h"

int parse_elf_class(const unsigned char* ei_class) {
    if (*ei_class == ELFCLASSNONE) /* invalid class = 0 */
        return (-1);
    
    int ret = -1;
    (*ei_class == ELFCLASS32) ? ret = ELFCLASS32 : 0; /* 32-bit objects = 1 */
    (*ei_class == ELFCLASS64) ? ret = ELFCLASS64 : 0; /* 64-bit objects = 2 */

    return (ret);     
}

int parse_elf_endian(const unsigned char* ei_data) {
    if (*ei_data == ELFDATANONE) /* invalid data encoding = 0 */
        return (-1);
    
    int ret = -1;
    (*ei_data == ELFDATA2LSB) ? ret = ELFDATA2LSB : 0; /* little endian = 1 */
    (*ei_data == ELFDATA2MSB) ? ret = ELFDATA2MSB : 0; /* big endian  = 2 */

    return ret;
}

int parse_elf_type(int type) {
    if (type == ET_NONE) /* invalid type = 0 */
        return (-1);

    int ret = -1;
    (type == ET_EXEC) ? ret = ET_EXEC : 0; /* Executable file */
    (type == ET_DYN) ? ret = ET_DYN : 0;   /* Shared object file : .so */
    (type == ET_REL) ? ret = ET_REL : 0;   /* Relocatable file : .o */

    return (ret);
}

int parse_elf_version(const unsigned char* version) {
    if (*version == EV_NONE)
        return (-1);

    int ret = -1;
    (*version == EV_CURRENT) ? ret = EV_CURRENT : 0;

    return (ret);
}

char* parse_elf_header(t_file* file) {
	const Elf32_Ehdr* header = file->addr;

    /* Verification du binaire en format ELF */
    if (ft_memcmp(header, ELFMAG, SELFMAG) != SUCCESS)
        return ("Invalid file format");

    /* Verification de la class du binaire ELF */
    int class;
    if ((class = parse_elf_class(&header->e_ident[EI_CLASS])) == -1)
        return ("Invalid class");

    /* Verification du type d'encodage du binaire ELF */
    if ((file->endian = parse_elf_endian(&header->e_ident[EI_DATA])) == -1)
        return ("Invalid data encoding");

    /* Verification de la version du fichier ELF */
    if (header->e_ident[EI_VERSION] != EV_CURRENT)
        return ("Invalid file version");

    /* Verification du type du binaire ELF */
    int type;
    if ((type = parse_elf_type(header->e_type)) == -1)
        return ("Invalid object file type");
        
    /* Lancement du gestionnaire du binaire en fonction de l'architecture */
    char* error;
    error = (class == ELFCLASS32) ? elf_x86_32_handler(file) : elf_x64_handler(file);
    if (error)
        return (error);

    return (NULL);
}