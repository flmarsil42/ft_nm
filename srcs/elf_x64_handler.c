#include "../includes/ft_nm.h" 
#include <stdint.h>
// #include <cstdint

void print_symbol_struct(Elf64_Sym* symbol) {
        printf("------------------------------------\n");
        printf("name         = %zu\n", (uint64_t)symbol->st_name); // decallage dans la .strtab ou commence le nom du symbole jusqu'au /0
        printf("info type    = %u\n", ELF64_ST_TYPE(symbol->st_info));
        printf("info bind    = %u\n", ELF64_ST_BIND(symbol->st_info));
        printf("other        = %u\n", symbol->st_other);
        printf("shndx        = %u\n", (uint16_t)symbol->st_shndx);
        printf("value        = %zu\n", (Elf64_Addr)symbol->st_value);
        printf("size         = %zu\n", (uint64_t)symbol->st_size);   
        
}


unsigned char elf_x64_get_symbol_info(t_elf_64* elf, Elf64_Sym* symbol) {
    
    unsigned char   c = '?'; // symbol inconnu
    uint64_t        st_info = symbol->st_info;
    uint64_t        st_shndx = symbol->st_shndx;
    uint64_t        sh_type = elf->shdr[st_shndx].sh_type; // section type
    uint64_t        sh_flags = elf->shdr[st_shndx].sh_flags; // section flags


    if (st_shndx == SHN_ABS) // valeur absolue
        c = 'A';
    else if (st_shndx == SHN_COMMON) // symbol commun 
        c = 'C';
    else if (st_shndx == STT_GNU_IFUNC) // reference indirecte vers un autre symbol
        c = 'I';
    else if (sh_flags == 0) // symbol de debug
        c = 'N';
    else if (sh_flags == 0) // symbol dans une section de donnees en lecture seule
        c = 'R';

    else if (ELF64_ST_BIND(st_info) == STB_LOCAL && c != '?') // lowercase
        c += 32;
    return (c);
}

int elf_x64_symbols_handler(t_elf_64* elf, t_file* file, int symtab_id) {
    printf("\033[36;01m.symbols handler : \n\033[00m");
    uint8_t*        mem = file->addr;

    unsigned int    strtab_idx      = elf->shdr[symtab_id].sh_link; // get strtab idx
    char*           symbol_table    = (char*) &mem[elf->shdr[strtab_idx].sh_offset];
    char*           string_table    = (char*) &mem[elf->shdr[elf->ehdr->e_shstrndx].sh_offset];
    Elf64_Sym*      symtab          = (Elf64_Sym*) &mem[elf->shdr[symtab_id].sh_offset];
    size_t          symbols_num     = elf->shdr[symtab_id].sh_size / elf->shdr[symtab_id].sh_entsize;
    unsigned int    section_name    = 0;

    (void)string_table;
    (void)section_name;

    /* Itere a travers chaque symbole de .symtab */
    for (size_t idx = 1 ; idx <= symbols_num; idx++) {
        if (ELF64_ST_TYPE(symtab[idx].st_info) == STT_FILE
            || ELF64_ST_TYPE(symtab[idx].st_info) == STT_SECTION)
            continue ;


        // print_symbol_struct(&symtab[idx]);
        elf_x64_get_symbol_info(elf, &symtab[idx]);


        printf("------------------------------------\n");
        section_name = elf->shdr[symtab[idx].st_shndx].sh_name;

        printf("Section name = %s\n", &string_table[section_name]);
        printf("Symbol value = %s\n", &symbol_table[symtab[idx].st_name]);

    }

    // printf("STT_NOTYPE = %d\n", STT_NOTYPE);
    // printf("STT_OBJECT = %d\n", STT_OBJECT);
    // printf("STT_FUNC = %d\n\n", STT_FUNC);

    // printf("STB_LOCAL = %d\n", STB_LOCAL);
    // printf("STB_GLOBAL = %d\n", STB_GLOBAL);
    // printf("STB_WEAK = %d\n", STB_WEAK);


    return (SUCCESS);
}


int elf_x64_sections_handler(t_file* file, t_elf_64* elf) {
    printf("\033[34;01msections handler :\n\033[00m");
    (void)file;
    
    size_t ret = -1;

    /* Itere a travers chaque section */
    for (size_t idx = 0; idx < elf->ehdr->e_shnum; idx++) {
        if (elf->shdr[idx].sh_type != SHT_SYMTAB)
            continue ;
        
        if (elf->shdr[idx].sh_link > elf->ehdr->e_shnum)
            return (ret);
        
        // verify endian swap ?

        ret = idx;
        break ;
    }

    return ret;
}

char* elf_x64_handler(t_file* file) {
    printf("\033[35;01mx64 handler :\n\033[00m");
    
    uint8_t* mem = file->addr;
    t_elf_64 elf;
    int symbol_table_idx;

    /* Debut du header ELF */
    elf.ehdr = (Elf64_Ehdr*)mem;
    /* Debut du header Program */
    elf.phdr = (Elf64_Phdr*)&mem[elf.ehdr->e_phoff];
    /* Debut du header Section */
    elf.shdr = (Elf64_Shdr*)&mem[elf.ehdr->e_shoff];

    // check header ???
    if ((symbol_table_idx = elf_x64_sections_handler(file, &elf)) && symbol_table_idx == -1) 
        return ("no symbols");
    else if (elf_x64_symbols_handler(&elf, file, symbol_table_idx) == -1)
        return ("Error during symbols handling");    

    return (SUCCESS);
}


    // printf("%s: 0x%lx\n", section_name, elf->shdr[i].sh_addr);



    /* Section contenant la table des chaînes de caractères. */
    // char* section_string_table = (char*)&mem[elf->shdr[elf->ehdr->e_shstrndx].sh_offset];
    // char* section_name;
    // section_name = &section_string_table[elf->shdr[i].sh_name];

  /* Imprime le nom et l'adresse de chaque en-tête de section. */
        // printf("%s: 0x%lx\n", &section_string_table[elf->shdr[i].sh_name], elf->shdr[i].sh_addr);

    // char* interp;

    // printf("\033[34;01mProgram header list\n\033[00m");
    // for (int i = 0; i < elf.ehdr->e_phnum; i++) {
    //     switch (elf.phdr[i].p_type) {
    //         case PT_LOAD:
    //             if (elf.phdr[i].p_offset == 0)
    //                 printf("Text segment: 0x%lx\n", elf.phdr[i].p_vaddr);
    //             else
    //                 printf("Data segment: 0x%lx\n", elf.phdr[i].p_vaddr);
    //             break;
    //         case PT_INTERP:
    //            interp = strdup((char *)&mem[elf.phdr[i].p_offset]);
    //            printf("Interpreter: %s\n", interp);
    //            break;
    //         case PT_NOTE:
    //            printf("Note segment: 0x%lx\n", elf.phdr[i].p_vaddr);
    //            break;
    //         case PT_DYNAMIC:
    //            printf("Dynamic segment: 0x%lx\n", elf.phdr[i].p_vaddr);
    //            break;
    //         case PT_PHDR:
    //            printf("Phdr segment: 0x%lx\n", elf.phdr[i].p_vaddr);
    //            break;
    //     }
    // }